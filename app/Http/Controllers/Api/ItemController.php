<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemStock;
use Illuminate\Http\Request;
use App\Models\backoffice_item_search;
use App\Models\item_stock_line;
use Carbon\Carbon;

class ItemController extends BaseController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */

    public function index()
    {
        $this->isUserActive();

        $this->validateRequest([
            'per_page' => 'required|integer|min:1',
            'page' => 'required|integer|min:1',
        ]);

        $user = auth()->guard('api')->user();
        $user_location = $user['UserLocation'];

        $stat = [1,2];
        if (isset($this->request->Status)){
            $itemstatus = $this->request->Status;
            if ($itemstatus==3){
                $stat = [1,2];
            }
            else{
                $stat = [$itemstatus];
            }
        }


        $created_by = $user['Unique'];
        return jsonResponse('Success', [
            'stocks' => Item::whereIn('Status',$stat)->where('CreatedBy',$created_by)->with('items')->orderBy('Unique','DESC')->forPage($this->request->page,$this->request->per_page)->get()
            // 'stocks' => Item::whereIn('Status',$stat)->where('CreatedBy',$created_by)->where('Location',
            // $user_location)->with('items')->orderBy('Unique','DESC')->forPage($this->request->page,$this->request->per_page)->get()
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store()
    {
        $this->isUserActive();

        $this->validateRequest([
            //'Location' => 'required',
            'Invoice' => 'required|string|max:250',
            'Notes' => 'nullable|string|max:250',
            'Station' => 'nullable|integer',
            'Status' => 'required|integer',
            'items' => 'array'
        ]);

        /* GET USER LOCATION */
        $user = auth()->guard('api')->user();
        $user_location = $user['UserLocation'];

        $input = $this->request->only(['Location', 'Invoice', 'Notes', 'Station', 'Status']);

        $input['Location'] = $user_location;
        $input['Updated'] = Null;
        $stock = Item::forceCreate($input);

        if ($this->request->items) {
            foreach ($this->request->items as $item) {

                $stock->items()->create([
                    //'Location' => $item['location'] ?? '',
                    'Updated' => Null,
                    'Item' => $item['item'],
                    'Location' => $user_location,
                    'Part' => $item['part'] ?? '',
                    'Description' => $item['description'],
                    'Quantity' => $item['quantity'],
                    'Status' => $item['status'],
                    'ItemUnique' => $item['unique'],
                    'Item' =>  $item['item'],
                ]);
            }
        }

        return jsonResponse('Stock created successfully.', [
            'stock' => $stock->fresh()->load('items')
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */


    public function update(Request $request)
    {
        $this->isUserActive();

        $this->validateRequest([
            'Unique' => 'required|integer|exists:item_stock_adjust_mobile',
            //'Location' => 'required',
            'Invoice' => 'required|string|max:250',
            'Notes' => 'nullable|string|max:250',
            'Station' => 'nullable|integer',
            'Status' => 'required|integer',
            'items' => 'nullable|array'
        ]);

        /* GET USER LOCATION */
        $user = auth()->guard('api')->user();
        $user_location = $user['UserLocation'];

        $stock = Item::findOrFail($this->request->Unique);

        $input = $this->request->only(['Location', 'Invoice', 'Notes', 'Station', 'Status','ItemStatus']);
        $input['Location'] = $user_location;

        $stock->update($input);

        if ($this->request->items) {

            foreach ($this->request->items as $items) {

                $stock_items_find = ItemStock::where('ItemUnique',$items['unique'])->where('AdjustUnique',
                    $this->request->Unique)->first();

                if ($stock_items_find){

                    $remove_item_stock_line = item_stock_line::where('StockAdjustDetailMobile',$stock_items_find['Unique'])->delete();

                    $stock_items = $stock_items_find->update([
                        'Location' => $items['location'] ?? '',
                        'Part' => $items['part'] ?? '',
                        'Description' => $items['description'],
                        'Quantity' => $items['quantity'],
                        'Status' => $items['status'],
                        'ItemUnique' => $items['unique'],
                        'Item' =>$items['item'],
                    ]);

                    if ($request->Status==2) {
                        $data['ItemUnique'] = $stock_items_find['ItemUnique'];
                        $data['LocationUnique'] = $stock_items_find['Location'];
                        $data['Type'] = 6;
                        $data['Quantity'] = $stock_items_find['Quantity'];
                        $data['Created'] = Carbon::now();
                        $data['CreatedBy'] = $stock_items_find['CreatedBy'];
                        $data['TransactionDate'] = Carbon::now();;
                        $data['status'] = 1;
                        $data['trans_date'] = Carbon::now();;
                        $data['TransID'] = $stock_items_find['AdjustUnique'];
                        $data['StockAdjustDetailMobile'] = $stock_items_find['Unique'];
                        $data['StockAdjustMobile'] = $stock_items_find['AdjustUnique'];

                        item_stock_line::create($data);
                    }
                }
                else{

                    $stock_item = $stock->items()->create([
                        //'Unique' => $items['unique'],
                        'Location' => $items['location'] ?? '',
                        'Part' => $items['part'] ?? '',
                        'Description' => $items['description'],
                        'Quantity' => $items['quantity'],
                        'Status' => $items['status'],
                        'UpdatedBy' => auth()->guard('api')->id(),
                        'ItemUnique' => $items['unique'],
                        'Item' =>  $items['item'],
                    ]);

                    if ($request->Status==2) {

                        $data['ItemUnique'] = $stock_item['ItemUnique'];
                        $data['LocationUnique'] = $stock_item['Location'];
                        $data['Type'] = 6;
                        $data['Quantity'] = $stock_item['Quantity'];
                        $data['Created'] = Carbon::now();
                        $data['CreatedBy'] = $stock_item['CreatedBy'];
                        $data['TransactionDate'] = Carbon::now();;
                        $data['status'] = 1;
                        $data['trans_date'] = Carbon::now();;
                        $data['TransID'] = $stock_item['AdjustUnique'];
                        $data['StockAdjustDetailMobile'] = $stock_item['Unique'];
                        $data['StockAdjustMobile'] = $stock_item['AdjustUnique'];

                        item_stock_line::create($data);
                    }
                }
            }
        }

        return jsonResponse('Stock updated successfully.', [
            'stock' => $stock->fresh()->load('items')
        ]);
    }

    public function delete()
    {
        $this->isUserActive();

        $this->validateRequest([
            'Unique' => 'required|integer|exists:item_stock_adjust_mobile',
        ]);


        $stock = Item::findOrFail($this->request->Unique);
        $input['Status'] = 0;
        $stock->update($input);

        // $update = ['Status' => '0','UpdatedBy'=>auth()->guard('api')->id()];
        $update = ['Status' => '0'];
        $stock_items = ItemStock::where('AdjustUnique',$this->request->Unique)->update($update);

        return jsonResponse('Stock deleted successfully.', [
            'stock' => $stock->fresh()->load('items')
        ]);
    }
}

