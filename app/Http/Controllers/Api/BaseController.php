<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BaseController extends Controller
{
    /**
     * @var array|\Illuminate\Contracts\Foundation\Application|Request|string
     */
    protected $request;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->request = request();
    }

    /**
     * @param array $rules
     * @param array $messages
     * @throws \Exception
     */
    public function validateRequest(array $rules, array $messages = []): void
    {
        $validator = Validator::make($this->request->all(), $rules, $messages);

        if ($validator->fails()) {
            throw new \Exception($validator->errors()->first());
        }
    }

    /**
     * @throws \Exception
     */
    public function isUserActive()
    {
        if ($this->user()->Status != 1) {
            throw new \Exception('User account is not active');
        }

        return true;
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
        return $this->guard()->user();
    }

    /**
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    public function guard()
    {
        return auth()->guard('api');
    }
}
