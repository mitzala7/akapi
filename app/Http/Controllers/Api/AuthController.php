<?php

namespace App\Http\Controllers\Api;

use App\Models\config_location_setting;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function login()
    {
        $this->validateRequest([
            'username' => 'required|string|exists:config_user,UserName',
            'password' => 'required|string'
        ]);

        $fields = ['Unique', 'UserName', 'FirstName', 'LastName', 'UserLocation', 'Password','PasswordMobile'];

        if ($user = User::where('UserName', $this->request->username)->where('PasswordMobile','!=','')->where('Status', 1)->first($fields)) {
            if (md5($this->request->password) != $user->Password) {
                return jsonResponse('Invalid password');
            }
            $token = $this->guard()->login($user);

            return jsonResponse('Success', [
                'token' => $token,
                'type' => 'Bearer',
                'user' => $user,
                'expires_in' => $this->guard()->factory()->getTTL() * 60
            ]);
        } else {
            return jsonResponse('Invalid Credentials provided.', [], false);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return jsonResponse('Success', [
            'token' => $this->guard()->refresh(),
            'type' => 'Bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return jsonResponse('User has been logged out.');
    }

    public function profile_update(){

        $this->isUserActive();
        $id = auth()->guard('api')->id();
        $user = User::findorFail($id);

        $input = $this->request->all();

        if (isset($input['Password']) && $input['Password']!=""){
            $input['Password'] = md5($input['Password']);
        }

        $user->update($input);

        return jsonResponse('User Profile updated successfully.', [
            'User' => $user->fresh()
        ]);
    }

    public function create_user_station()
    {
        $this->isUserActive();

        $this->validateRequest([
            'Setting' => 'required|string|max:250',
            'Value' => 'nullable|string|max:250',
            'Notes' => 'nullable|string',
            'Status' => 'required|boolean',
        ]);

        /* GET USER LOCATION */
        $user = auth()->guard('api')->user();
        $user_station_unique = $user['UserStation'];
        $input = $this->request->all();

        $check_station_unique = config_location_setting::where('stationunique',$user_station_unique)->first();
        if ($check_station_unique){
            return jsonResponse('User Station created successfully.', [
                'User_station' => $check_station_unique->fresh()
            ]);
        }
        else{
            $input['stationunique'] = $user_station_unique;
            $input['CreatedBy'] = $user['Unique'];
            $input['UpdatedBy'] = $user['Unique'];
            $stock = config_location_setting::forceCreate($input);

            return jsonResponse('User Station created successfully.', [
                'User_station' => $stock->fresh()
            ]);
        }
    }

    public function update_user_station()
    {
        $this->isUserActive();

        $this->validateRequest([
            'Unique' => 'required|integer|exists:config_location_settings',
        ]);
        /* GET USER LOCATION */
        $user = auth()->guard('api')->user();
        $user_station_unique = $user['UserStation'];

        $setting = config_location_setting::findOrFail($this->request->Unique);
        $input = $this->request->all();
        $input['stationunique'] = $user_station_unique;

        $setting->update($input);

        return jsonResponse('User Station updated successfully.', [
            'User_station' => $setting->fresh()
        ]);
    }

    public function get_user_station()
    {
        $this->isUserActive();

        $this->validateRequest([
            'per_page' => 'required|integer|min:1',
            'page' => 'required|integer|min:1',
        ]);

        $user = auth()->guard('api')->user();
        $user_station = $user['UserStation'];

        return jsonResponse('Success', [


            'User_station' => config_location_setting::where('Status', 1)->where('Setting','MobileSettings')->where('stationunique',
                $user_station)->orderBy('Unique','DESC')->forPage($this->request->page,$this->request->per_page)->get(),

            'Location' => config_location_setting::select('Unique','LocationName')->where('Status', 1)
                ->orderBy('Unique','ASC')->get()
        ]);
    }
}
