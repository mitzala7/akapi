<?php

namespace App\Http\Controllers\Api;

use App\Models\ItemStock;

class ItemStockController extends BaseController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index()
    {
        $this->validateRequest([
            'search' => 'nullable|string',
            'stockUnique' => 'nullable|numeric',
            'perPage' => 'nullable|numeric|min:1',
            'page' => 'nullable|numeric|min:1'
        ]);

        $query = ItemStock::query();

        if ($this->request->search) {
            $query->where('Description', 'iLIKE', "%{$this->request->search}%");
        }

        if ($this->request->stockUnique) {
            $query->where('AdjustUnique', $this->request->stockUnique);
        }

        return jsonResponse('Success', [
            'items' => $query->where('Status',1)->forPage($this->request->page ?: 1,$this->request->perPage ?: 20)
                ->get()
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update()
    {
        $this->isUserActive();

        $this->validateRequest([
            'Unique' => 'required|integer|exists:item_stock_adjust_mobile_details',
            'Location' => 'required',
            'ItemUnique' => 'required|integer',
            'Item' => 'required|integer',
            'Part' => 'required|integer',
            'Description' => 'required|string|max:250',
            'Status' => 'required|boolean'
        ]);

        $stockItem = ItemStock::findOrFail($this->request->Unique);

        $stockItem->forceFill($this->request->only(['Unique', 'Location', 'ItemUnique', 'Item', 'Part', 'Description', 'Status']));
        $stockItem->save();

        return jsonResponse('Success', [
            'stock_item' => $stockItem
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete()
    {
        $this->isUserActive();

        $this->validateRequest([
            'itemUnique' => 'required|numeric'
        ]);

        $update = ['Status' => '0'];
        ItemStock::where('Unique',$this->request->itemUnique)->update($update);

        return jsonResponse('Item deleted successfully.');
    }
}
