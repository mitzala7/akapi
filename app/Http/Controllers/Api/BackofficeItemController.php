<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\backoffice_item_search;
use App\Models\ItemStock;
use Illuminate\Http\Request;

class BackofficeItemController extends BaseController
{
    public function index()
    {
        $this->validateRequest([
            'search' => 'nullable|string',
            'perPage' => 'nullable|numeric|min:1',
            'page' => 'nullable|numeric|min:1'
        ]);

        $query = backoffice_item_search::select('Unique','Item','Part','Description','CostLanded','Stock0','Stock1','Stock2','Stock3','Stock4','Stock5','Stock6','Stock7','Stock8','Stock9','Stock10');

        $keyword = $this->request->search;
        if ($this->request->search) {
            $query->where('Status', 1);
            $query->Where(function ($query1) use ($keyword){
                $query1->where('Item', 'iLIKE', "%{$keyword}%");
                $query1->orwhere('Description', 'iLIKE', "%{$keyword}%");
                $query1->orwhere('Part', 'iLIKE', "%{$keyword}%");
            });
        }

        return jsonResponse('Success', [
            'items' => $query->forPage($this->request->page ?: 1,$this->request->perPage ?: 20)->get()
        ]);
    }
}
