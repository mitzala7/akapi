<?php

use Illuminate\Http\JsonResponse;

if (!function_exists('jsonResponse')) {
    /**
     * json response for api calls
     *
     * @param $message
     * @param array $data
     * @param bool $type
     *
     * @return JsonResponse
     */
    function jsonResponse(string $message, $data = [], bool $type = true)
    {
        return new JsonResponse(['status' => $type, 'message' => $message, 'data' => $data]);
    }
}