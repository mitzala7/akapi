<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class item_stock_line extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

//        self::creating(function (self $item) {
//            //$item->AdjustDate = now()->format('Y-m-d H:i:s');
//            $item->CreatedBy = auth()->guard('api')->id();
//        });

        self::updating(function (self $item) {
            if ($item->isDirty()) $item->UpdatedBy = auth()->guard('api')->id();
        });
    }

    protected $table = 'item_stock_line';

    protected $primaryKey = 'Unique';

    public const CREATED_AT = 'Created';

    public const UPDATED_AT = 'Updated';
}
