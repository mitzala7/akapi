<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        self::creating(function (self $item) {
            $item->AdjustDate = now()->format('Y-m-d H:i:s');
            $item->CreatedBy = auth()->guard('api')->id();
        });

        self::updating(function (self $item) {
            if ($item->isDirty()) $item->UpdatedBy = auth()->guard('api')->id();
        });
    }

    protected $table = 'item_stock_adjust_mobile';

    protected $primaryKey = 'Unique';

    public const CREATED_AT = 'Created';

    public const UPDATED_AT = 'Updated';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(ItemStock::class, 'AdjustUnique');
    }
}
