<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api']], function ($router) {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
        Route::post('logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
        Route::post('refresh', [\App\Http\Controllers\Api\AuthController::class, 'refresh']);
        Route::post('profile_update', [\App\Http\Controllers\Api\AuthController::class, 'profile_update']);
    });

    Route::group(['middleware' => 'auth:api'], function () {


        Route::post('create_user_station', [\App\Http\Controllers\Api\AuthController::class, 'create_user_station']);
        Route::post('update_user_station', [\App\Http\Controllers\Api\AuthController::class, 'update_user_station']);
        Route::post('get_user_station', [\App\Http\Controllers\Api\AuthController::class, 'get_user_station']);

        Route::post('stocks', [\App\Http\Controllers\Api\ItemController::class, 'index']);
        Route::post('stocks/create', [\App\Http\Controllers\Api\ItemController::class, 'store']);
        Route::post('stocks/update', [\App\Http\Controllers\Api\ItemController::class, 'update']);
        Route::post('stocks/delete', [\App\Http\Controllers\Api\ItemController::class, 'delete']);

        Route::post('stock/items', [\App\Http\Controllers\Api\ItemStockController::class, 'index']);
        Route::post('stock/item/update', [\App\Http\Controllers\Api\ItemStockController::class, 'update']);
        Route::post('stock/item/delete', [\App\Http\Controllers\Api\ItemStockController::class, 'delete']);

        Route::post('stock/backoffice/items', [\App\Http\Controllers\Api\BackofficeItemController::class, 'index']);

    });
});