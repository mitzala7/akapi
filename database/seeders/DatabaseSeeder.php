<?php

namespace Database\Seeders;

use App\Models\backoffice_item_search;
use App\Models\config_location_setting;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'UserName' => 'mit',
            'FirstName' => 'zala',
            'LastName' => 'zala',
            'Password' => md5(123456),
            'UserLocation' => '6',
            'UserStation' => '1',
            'PasswordMobile' => md5(123456),
            'Status' => 1
        ]);

        backoffice_item_search::create([
            'Item' => 'WES30WM',
            'Part' => '12',
            'Description' => 'tester',
            'CostLanded' => '0.000000',
            'Stock0' => '0',
            'Stock1' => '0',
            'Stock2' => '0',
            'Stock3' => '0',
            'Stock4' => '0',
            'Stock5' => '0',
            'Stock6' => '0',
            'Stock7' => '0',
            'Stock8' => '0',
            'Stock9' => '0',
            'Stock10' => '0',
            'Status' => 1
        ]);

        config_location_setting::create([
            'LocationUnique' => '1',
            'stationunique' => '1',
            'Setting' => 'MobileSettings',
            'Status' => 1,
            'Created' => '2020-11-06 22:43:00',
            'UUID' => 'Tester',
            'SyncStatus' => 1,
        ]);

        config_location_setting::create([
            'LocationUnique' => '1',
            'stationunique' => '2',
            'Setting' => 'MobileSettings',
            'Status' => 1,
            'Created' => '2020-11-06 22:43:00',
            'UUID' => 'Tester',
            'SyncStatus' => 1,
        ]);

        config_location_setting::create([
            'LocationUnique' => '1',
            'stationunique' => '3',
            'Setting' => 'MobileSettings',
            'Status' => 1,
            'Created' => '2020-11-06 22:43:00',
            'UUID' => 'Tester',
            'SyncStatus' => 1,
        ]);
    }
}
