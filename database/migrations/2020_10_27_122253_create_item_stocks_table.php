<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_stock_adjust_mobile_details', function (Blueprint $table) {
            $table->id('Unique');
            $table->unsignedBigInteger('AdjustUnique');
            $table->string('Location')->nullable();
            $table->unsignedBigInteger('ItemUnique')->nullable();
            $table->unsignedBigInteger('Item')->nullable();
            $table->unsignedBigInteger('Part')->nullable();
            $table->string('Description');
            $table->decimal('Quantity', 8, 4)->default(0);
            $table->boolean('Status')->default(0);
            $table->unsignedBigInteger('CreatedBy')->nullable();
            $table->unsignedBigInteger('UpdatedBy')->nullable();
            $table->timestamp('Created')->nullable();
            $table->timestamp('Updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_stocks');
    }
}
