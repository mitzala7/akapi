<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackofficeItemSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backoffice_item_search', function (Blueprint $table) {
                $table->id('ID');
                $table->unsignedBigInteger('Unique')->nullable();
                $table->string('Item')->nullable();
                $table->unsignedBigInteger('Part')->nullable();
                $table->string('Description')->nullable();
                $table->decimal('CostLanded', 8, 4)->default(0);
                $table->unsignedBigInteger('Stock0')->nullable();
                $table->unsignedBigInteger('Stock1')->nullable();
                $table->unsignedBigInteger('Stock2')->nullable();
                $table->unsignedBigInteger('Stock3')->nullable();
                $table->unsignedBigInteger('Stock4')->nullable();
                $table->unsignedBigInteger('Stock5')->nullable();
                $table->unsignedBigInteger('Stock6')->nullable();
                $table->unsignedBigInteger('Stock7')->nullable();
                $table->unsignedBigInteger('Stock8')->nullable();
                $table->unsignedBigInteger('Stock9')->nullable();
                $table->unsignedBigInteger('Stock10')->nullable();
                $table->boolean('Status')->default(0);
                $table->timestamp('Created')->nullable();
                $table->timestamp('Updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('backoffice_item_search', function (Blueprint $table) {
            //
        });
    }
}
