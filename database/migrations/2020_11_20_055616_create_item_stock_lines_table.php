<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemStockLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_stock_line', function (Blueprint $table) {
            $table->id('Unique');
            $table->unsignedBigInteger('ItemUnique')->nullable();
            $table->unsignedBigInteger('LocationUnique')->nullable();
            $table->unsignedBigInteger('Type')->nullable();
            $table->decimal('Quantity', 8, 4)->default(0);
            $table->unsignedBigInteger('CreatedBy')->nullable();
            $table->unsignedBigInteger('UpdatedBy')->nullable();
            $table->timestamp('Created')->nullable();
            $table->timestamp('Updated')->nullable();
            $table->timestamp('TransactionDate')->nullable();
            $table->text('Comment')->nullable();
            $table->unsignedBigInteger('receiptdetailsunique')->nullable();
            $table->boolean('status')->default(0);
            $table->unsignedBigInteger('receipt_header_unique')->nullable();
            $table->decimal('unit_cost', 8, 4)->default(0);
            $table->unsignedBigInteger('purchase_details_unique')->nullable();
            $table->date('trans_date')->nullable();
            $table->unsignedBigInteger('WWUnique')->nullable();
            $table->decimal('Cost', 8, 4)->default(0);
            $table->decimal('CostExtra', 8, 4)->default(0);
            $table->decimal('CostFreight', 8, 4)->default(0);
            $table->decimal('CostDuty', 8, 4)->default(0);
            $table->unsignedBigInteger('CountUnique')->nullable();
            $table->unsignedBigInteger('TransferUnique')->nullable();
            $table->unsignedBigInteger('TransID')->nullable();
            $table->text('StockAdjustDetail')->nullable();
            $table->unsignedBigInteger('LocationStock')->nullable();
            $table->unsignedBigInteger('TotalStock')->nullable();
            $table->unsignedBigInteger('UUID')->nullable();
            $table->unsignedBigInteger('ReceiptDetailsUniqueStore')->nullable();
            $table->unsignedBigInteger('ReceiptHeaderUniqueStore')->nullable();
            $table->unsignedBigInteger('TransIDStore')->nullable();
            $table->unsignedBigInteger('ReceiptHeaderUUID')->nullable();
            $table->unsignedBigInteger('ReceiptDetailsUUID')->nullable();
            $table->unsignedBigInteger('PurchaseUUID')->nullable();
            $table->unsignedBigInteger('PurchaseDetailsUUID')->nullable();
            $table->unsignedBigInteger('TransferUUID')->nullable();
            $table->unsignedBigInteger('TransferDetailsUUID')->nullable();
            $table->unsignedBigInteger('TransUUID')->nullable();
            $table->unsignedBigInteger('CreateByUUID')->nullable();
            $table->unsignedBigInteger('UpdateByUUID')->nullable();
            $table->timestamp('SyncDate')->nullable();
            $table->text('CreateLocation')->nullable();
            $table->unsignedBigInteger('UniqueHQ')->nullable();
            $table->unsignedBigInteger('UniqueStore')->nullable();
            $table->unsignedBigInteger('StockAdjustUUID')->nullable();
            $table->unsignedBigInteger('StockAdjustDetailsUUID')->nullable();
            $table->boolean('SyncStatus')->default(0);
            $table->unsignedBigInteger('PurchaseDetailsUniqueStore')->nullable();
            $table->unsignedBigInteger('TransferDetailsUniqueStore')->nullable();
            $table->unsignedBigInteger('StockAdjustDetailUniqueStore')->nullable();
            $table->boolean('ReceiptHeaderStatus')->default(0);
            $table->unsignedBigInteger('CountUniqueStore')->nullable();
            $table->decimal('ItemKitQuantity', 8, 4)->default(0);
            $table->unsignedBigInteger('SyncManual')->nullable();
            $table->unsignedBigInteger('ItemParentUnique')->nullable();
            $table->unsignedBigInteger('PurchaseUnique')->nullable();
            $table->unsignedBigInteger('ItemSerialUnique')->nullable();
            $table->unsignedBigInteger('LedgerAccount')->nullable();
            $table->unsignedBigInteger('ReceiveUnique')->nullable();
            $table->unsignedBigInteger('ReceiveDetailUnique')->nullable();
            $table->text('CreateStation')->nullable();
            $table->unsignedBigInteger('StockAdjustDetailMobile')->nullable();
            $table->unsignedBigInteger('StockAdjustMobile')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_stock_line');
    }
}
