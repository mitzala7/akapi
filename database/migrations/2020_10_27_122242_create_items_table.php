<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_stock_adjust_mobile', function (Blueprint $table) {
            $table->id('Unique');
            $table->dateTime('AdjustDate');
            $table->string('Location')->nullable();
            $table->string('Invoice');
            $table->string('Notes')->nullable();
            $table->boolean('Station')->nullable();
            $table->integer('Status')->default(0)->comment("0=delete,1=Pending, 2=Complete");
            $table->unsignedBigInteger('CreatedBy')->nullable();
            $table->unsignedBigInteger('UpdatedBy')->nullable();
            $table->timestamp('Created')->nullable();
            $table->timestamp('Updated')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
