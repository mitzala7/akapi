<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigLocationSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_location_settings', function (Blueprint $table) {
            $table->id('Unique');
            $table->unsignedBigInteger('LocationUnique')->nullable();
            $table->string('LocationName')->nullable();
            $table->unsignedBigInteger('stationunique')->nullable();
            $table->string('Setting')->nullable();
            $table->string('Value')->nullable();
            $table->string('Notes')->nullable();
            $table->boolean('Status')->default(0);
            $table->timestamp('Created')->nullable();
            $table->unsignedBigInteger('CreatedBy')->nullable();
            $table->timestamp('Updated')->nullable();
            $table->unsignedBigInteger('UpdatedBy')->nullable();
            $table->string('UUID')->nullable();
            $table->unsignedBigInteger('UpdatedByUUID')->nullable();
            $table->unsignedBigInteger('UniqueHQ')->nullable();
            $table->unsignedBigInteger('SyncStatus')->nullable();
            $table->timestamp('SyncDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config_location_settings', function (Blueprint $table) {
            //
        });
    }
}
