<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_user', function (Blueprint $table) {
            $table->id('Unique');
            $table->string('UserName');
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('Password');
            $table->string('PasswordMobile');
            $table->string('UserLocation');
            $table->unsignedBigInteger('UserStation')->nullable();
            $table->boolean('Status')->default(0);
            $table->timestamp('Created')->nullable();
            $table->timestamp('Updated')->nullable();
        });

//        Schema::table('config_user', function (Blueprint $table) {
//            $table->unsignedBigInteger('UserStation')->nullable();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
